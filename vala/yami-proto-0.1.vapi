/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
  Mumble to Mumble gateway
  Copyright (c) 2017 Phobos (promi) <prometheus@unterderbruecke.de>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
[CCode (cheader_filename = "yami-proto/yami-proto.h")]
namespace Yami
{
  [Compact]
  public class Version {
    uint16? major;
    uint8? minor;
    uint8? patch;
    string? release;
    string? os;
    string? os_version;
  }
  
  public enum UdpPayloadType {
    PING,
    OPUS_AUDIO
  }
  
  [Compact]
  public class UdpPacket {
    public UdpPacket ();
    public UdpPayloadType type;
    public uint8 target; // 0 = normal talking, 1-30 whisper, 31 = server loopback
    public uint32 source_session_id;
    public uint32 timestamp; // Only used for Ping
    public uint32 sequence_number;
    [CCode (array_length_cname = "payload_length", array_length_type = "size_t")]
    public uint8[] payload;
    public bool terminator; // Only used for Opus
  }

  public enum MessageType {
    INVALID,
    VERSION,
    UDP_TUNNEL,
    AUTHENTICATE,
    PING,
    REJECT,
    SERVER_SYNC,
    CHANNEL_REMOVE,
    CHANNEL_STATE,
    USER_REMOVE,
    USER_STATE,
    BAN_LIST,
    TEXT_MESSAGE,
    PERMISSION_DENIED,
    ACL,
    QUERY_USERS,
    CRYPT_SETUP,
    CONTEXT_ACTION_MODIFY,
    CONTEXT_ACTION,
    USER_LIST,
    VOICE_TARGET,
    PERMISSION_QUERY,
    CODEC_VERSION,
    USER_STATS,
    REQUEST_BLOB,
    SERVER_CONFIG,
    SUGGEST_CONFIG
  }

  public delegate bool WriteDataFunc (uint8[] data);
  public delegate void VersionFunc (Version version);
  public delegate void PingFunc ();
  public delegate void UdpTunnelFunc (UdpPacket packet);

  [Compact]
  public class Client {
    public Client (owned WriteDataFunc write_data_func);

    public uint get_next_read_size ();
    public bool consume_read_data (uint8[] data);
    public bool has_message ();
    public MessageType get_message_type ();

    public bool send_version ();
    public void recv_version (VersionFunc version_func);

    public bool send_authenticate (string username, string password, string[] tokens);

    public bool send_ping ();
    public void recv_ping (PingFunc ping_func);

    public bool send_udp_tunnel (UdpPacket packet);
    public void recv_udp_tunnel (UdpTunnelFunc udp_tunnel_func);

    public bool send_user_remove (uint32 session, uint32? actor, string reason, bool ban);
    public void recv_user_remove (UserRemoveFunc user_remove_func);
  }
}
