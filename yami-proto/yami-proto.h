/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
  Mumble to Mumble gateway
  Copyright (c) 2017 Phobos (promi) <prometheus@unterderbruecke.de>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#ifdef __cplusplus
extern "C"
{
#endif
#include <stddef.h>
#include <stdint.h>

  typedef struct YamiVersion_t
  {
    uint16_t const *major;
    uint8_t const *minor;
    uint8_t const *patch;
    char const *release;
    size_t release_length;
    char const *os;
    size_t os_length;
    char const *os_version;
    size_t os_version_length;
  } YamiVersion;
  
  typedef enum
    {
      YAMI_UDP_PAYLOAD_TYPE_CELT_ALPHA_AUDIO,
      YAMI_UDP_PAYLOAD_TYPE_PING,
      YAMI_UDP_PAYLOAD_TYPE_SPEEX_AUDIO,
      YAMI_UDP_PAYLOAD_TYPE_CELT_BETA_AUDIO,
      YAMI_UDP_PAYLOAD_TYPE_OPUS_AUDIO,
      YAMI_UDP_PAYLOAD_TYPE_UNUSED1,
      YAMI_UDP_PAYLOAD_TYPE_UNUSED2,
      YAMI_UDP_PAYLOAD_TYPE_UNUSED3
    } YamiUdpPayloadType;

  typedef struct YamiUdpPacket_t
  {
    YamiUdpPayloadType type;
    uint8_t target;
    uint32_t source_session_id;
    uint32_t timestamp;
    uint32_t sequence_number;
    uint8_t *payload;
    size_t payload_length;
    int terminator;
  } YamiUdpPacket;

  YamiUdpPacket* yami_udp_packet_new ();
  void yami_udp_packet_free (YamiUdpPacket *self);
  
  typedef enum
    {
      YAMI_MESSAGE_TYPE_INVALID,
      YAMI_MESSAGE_TYPE_VERSION,
      YAMI_MESSAGE_TYPE_UDP_TUNNEL,
      YAMI_MESSAGE_TYPE_AUTHENTICATE,
      YAMI_MESSAGE_TYPE_PING,
      YAMI_MESSAGE_TYPE_REJECT,
      YAMI_MESSAGE_TYPE_SERVER_SYNC,
      YAMI_MESSAGE_TYPE_CHANNEL_REMOVE,
      YAMI_MESSAGE_TYPE_CHANNEL_STATE,
      YAMI_MESSAGE_TYPE_USER_REMOVE,
      YAMI_MESSAGE_TYPE_USER_STATE,
      YAMI_MESSAGE_TYPE_BAN_LIST,
      YAMI_MESSAGE_TYPE_TEXT_MESSAGE,
      YAMI_MESSAGE_TYPE_PERMISSION_DENIED,
      YAMI_MESSAGE_TYPE_ACL,
      YAMI_MESSAGE_TYPE_QUERY_USERS,
      YAMI_MESSAGE_TYPE_CRYPT_SETUP,
      YAMI_MESSAGE_TYPE_CONTEXT_ACTION_MODIFY,
      YAMI_MESSAGE_TYPE_CONTEXT_ACTION,
      YAMI_MESSAGE_TYPE_USER_LIST,
      YAMI_MESSAGE_TYPE_VOICE_TARGET,
      YAMI_MESSAGE_TYPE_PERMISSION_QUERY,
      YAMI_MESSAGE_TYPE_CODEC_VERSION,
      YAMI_MESSAGE_TYPE_USER_STATS,
      YAMI_MESSAGE_TYPE_REQUEST_BLOB,
      YAMI_MESSAGE_TYPE_SERVER_CONFIG,
      YAMI_MESSAGE_TYPE_SUGGEST_CONFIG
    } YamiMessageType;
  
  typedef void (*UserDataFreeFunc) (void *user_data);

  typedef int (*YamiWriteDataFunc) (uint8_t const *data, int data_length,
                                    void *user_data);
  typedef void (*YamiVersionFunc) (YamiVersion *version, void *user_data);
  typedef void (*YamiPingFunc) (void *user_data);
  typedef void (*YamiUdpTunnelFunc) (YamiUdpPacket *packet,
                                     void *user_data);

  typedef struct YamiClient_t YamiClient;

  YamiClient* yami_client_new (YamiWriteDataFunc write_data_func,
                               void* user_data,
                               UserDataFreeFunc user_data_free_func);
  void yami_client_free (YamiClient *self);

  size_t yami_client_get_next_read_size (YamiClient *self);
  int yami_client_consume_read_data (YamiClient *self, const uint8_t *data,
                                     int data_length);
  int yami_client_has_message (YamiClient *self);
  YamiMessageType yami_client_get_message_type (YamiClient *self);

  int yami_client_send_version (YamiClient *self);
  void yami_client_recv_version (YamiClient *self,
                                 YamiVersionFunc version_func,
                                 void* user_data);
  int yami_client_send_authenticate (YamiClient *self,
                                     const char *username,
                                     const char *password,
                                     const char **tokens,
                                     const size_t tokens_length);
  int yami_client_send_ping (YamiClient *self);
  void yami_client_recv_ping (YamiClient *self, YamiPingFunc ping_func,
                              void *user_data);
  int yami_client_send_udp_tunnel (YamiClient *self,
                                   const YamiUdpPacket *udp_packet);
  void yami_client_recv_udp_tunnel (YamiClient *self,
                                    YamiUdpTunnelFunc udp_tunnel_func,
                                    void *user_data);

  void yami_client_send_user_remove (YamiClient *self,
                                     uint32_t session,
                                     uint32_t *actor,
                                     const char *reason,
                                     int reason_length,
                                     int ban);

  void yami_client_send_user_remove (YamiClient *self,
                                     uint32_t session,
                                     uint32_t *actor,
                                     const char *reason,
                                     int reason_length,
                                     int ban);

#ifdef __cplusplus
}
#endif
