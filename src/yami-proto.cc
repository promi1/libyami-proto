/* -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
  Mumble to Mumble gateway
  Copyright (c) 2017 Phobos (promi) <prometheus@unterderbruecke.de>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "log/logger.hh"
#include "mumble/client.hh"
#include "mumble/version.hh"

#include "yami-proto/yami-proto.h"

using namespace std::literals;

template<typename T>
T const* optional_val_to_ptr (const std::optional<T> &optval) {
  return optval ? &(*optval) : nullptr;
}

template<typename T>
std::pair<T const*, int> optional_string_to_ptr (const std::optional<std::basic_string<T>> &optval) {
  if (optval) {
    return std::make_pair(optval->data (), optval->length ());
  }
  else {
    return std::make_pair(nullptr, -1);
  }
}
  
extern "C"
{
  YamiUdpPacket* yami_udp_packet_new ()
  {
    return new YamiUdpPacket {};
  }
  
  void yami_udp_packet_free (YamiUdpPacket *self)
  {
    delete self;
  }
  
  struct YamiClient_t
  {
    std::unique_ptr<Log::ILogger> logger;
    std::unique_ptr<Mumble::IClient> data;
    void *user_data;
    UserDataFreeFunc user_data_free_func;
  };

  YamiClient* yami_client_new (YamiWriteDataFunc write_data_func,
                               void* user_data,
                               UserDataFreeFunc user_data_free_func)
  {
    YamiClient *self = new YamiClient ();
    self->logger = std::make_unique<Log::ConsoleLogger> ();
    self->user_data = user_data;
    self->user_data_free_func = user_data_free_func;
    self->data = std::make_unique<Mumble::Client> (*self->logger,
      [write_data_func, user_data] (const std::vector<uint8_t> &data)
      {
        if (write_data_func (data.data (), data.size (), user_data) == 0)
         {
           throw new std::runtime_error ("writing data failed");
         }
      });
    return self;
  }
  
  void yami_client_free (YamiClient *self)
  {
    if (self) {
      self->user_data_free_func (self->user_data);
      delete self;
    }
  }
  
  int yami_client_send_version (YamiClient *self)
  {
    if (self == nullptr) {
      return 0;
    }
    try
      {
        self->data->send_version ();
        return 1;
      }
    catch (const std::runtime_error &e)
      {
        return 0;
      }
  }

  void yami_client_recv_version (YamiClient *self,
                                 YamiVersionFunc version_func,
                                 void* user_data)
  {
    if (self == nullptr) {
      return;
    }
    Mumble::Version version = self->data->recv_version ();
    YamiVersion ver;
    ver.major = optional_val_to_ptr (version.major);
    ver.minor = optional_val_to_ptr (version.minor);
    ver.patch = optional_val_to_ptr (version.patch);
    std::tie (ver.release, ver.release_length) = optional_string_to_ptr (version.release);
    std::tie (ver.os, ver.os_length) = optional_string_to_ptr (version.os);
    std::tie (ver.os_version, ver.os_version_length) = optional_string_to_ptr (version.os_version);
    version_func (&ver, user_data);
  }
  
  size_t yami_client_get_next_read_size (YamiClient *self)
  {
    if (self == nullptr) {
      return 0;
    }
    return self->data->get_next_read_size ();
  }
  
  int yami_client_consume_read_data (YamiClient *self, const std::uint8_t *data, int data_length)
  {
    if (self == nullptr) {
      return 0;
    }
    try
      {
        self->data->consume_read_data (std::vector<std::uint8_t> {data, data + data_length});
        return 1;
      }
    catch (const std::runtime_error &e)
      {
        return 0;
      }
  }
  
  int yami_client_has_message (YamiClient *self)
  {
    if (self == nullptr) {
      return 0;
    }
    return self->data->has_message ();
  }

  YamiMessageType yami_client_get_message_type (YamiClient *self)
  {
    if (self == nullptr) {
      return YAMI_MESSAGE_TYPE_INVALID;
    }
    if (self->data->has_message ())
      {
        if (self->data->message_type () == Mumble::MessageType::Version)
          {
            return YAMI_MESSAGE_TYPE_VERSION;
          }
        else if (self->data->message_type () == Mumble::MessageType::UDPTunnel)
          {
            return YAMI_MESSAGE_TYPE_UDP_TUNNEL;
          }
        else if (self->data->message_type () == Mumble::MessageType::Authenticate)
          {
            return YAMI_MESSAGE_TYPE_AUTHENTICATE;
          }
        else if (self->data->message_type () == Mumble::MessageType::Ping)
          {
            return YAMI_MESSAGE_TYPE_PING;
          }
        else if (self->data->message_type () == Mumble::MessageType::Reject)
          {
            return YAMI_MESSAGE_TYPE_REJECT;
          }
        else if (self->data->message_type () == Mumble::MessageType::ServerSync)
          {
            return YAMI_MESSAGE_TYPE_SERVER_SYNC;
          }
        else if (self->data->message_type () == Mumble::MessageType::ChannelRemove)
          {
            return YAMI_MESSAGE_TYPE_CHANNEL_REMOVE;
          }
        else if (self->data->message_type () == Mumble::MessageType::ChannelState)
          {
            return YAMI_MESSAGE_TYPE_CHANNEL_STATE;
          }
        else if (self->data->message_type () == Mumble::MessageType::UserRemove)
          {
            return YAMI_MESSAGE_TYPE_USER_REMOVE;
          }
        else if (self->data->message_type () == Mumble::MessageType::UserState)
          {
            return YAMI_MESSAGE_TYPE_USER_STATE;
          }
        else if (self->data->message_type () == Mumble::MessageType::BanList)
          {
            return YAMI_MESSAGE_TYPE_BAN_LIST;
          }
        else if (self->data->message_type () == Mumble::MessageType::TextMessage)
          {
            return YAMI_MESSAGE_TYPE_TEXT_MESSAGE;
          }
        else if (self->data->message_type () == Mumble::MessageType::PermissionDenied)
          {
            return YAMI_MESSAGE_TYPE_PERMISSION_DENIED;
          }
        else if (self->data->message_type () == Mumble::MessageType::ACL)
          {
            return YAMI_MESSAGE_TYPE_ACL;
          }
        else if (self->data->message_type () == Mumble::MessageType::QueryUsers)
          {
            return YAMI_MESSAGE_TYPE_QUERY_USERS;
          }
        else if (self->data->message_type () == Mumble::MessageType::CryptSetup)
          {
            return YAMI_MESSAGE_TYPE_CRYPT_SETUP;
          }
        else if (self->data->message_type () == Mumble::MessageType::ContextActionModify)
          {
            return YAMI_MESSAGE_TYPE_CONTEXT_ACTION_MODIFY;
          }
        else if (self->data->message_type () == Mumble::MessageType::ContextAction)
          {
            return YAMI_MESSAGE_TYPE_CONTEXT_ACTION;
          }
        else if (self->data->message_type () == Mumble::MessageType::UserList)
          {
            return YAMI_MESSAGE_TYPE_USER_LIST;
          }
        else if (self->data->message_type () == Mumble::MessageType::VoiceTarget)
          {
            return YAMI_MESSAGE_TYPE_VOICE_TARGET;
          }
        else if (self->data->message_type () == Mumble::MessageType::PermissionQuery)
          {
            return YAMI_MESSAGE_TYPE_PERMISSION_QUERY;
          }
        else if (self->data->message_type () == Mumble::MessageType::CodecVersion)
          {
            return YAMI_MESSAGE_TYPE_CODEC_VERSION;
          }
        else if (self->data->message_type () == Mumble::MessageType::UserStats)
          {
            return YAMI_MESSAGE_TYPE_USER_STATS;
          }
        else if (self->data->message_type () == Mumble::MessageType::RequestBlob)
          {
            return YAMI_MESSAGE_TYPE_REQUEST_BLOB;
          }
        else if (self->data->message_type () == Mumble::MessageType::ServerConfig)
          {
            return YAMI_MESSAGE_TYPE_SERVER_CONFIG;
          }
        else if (self->data->message_type () == Mumble::MessageType::SuggestConfig)
          {
            return YAMI_MESSAGE_TYPE_SUGGEST_CONFIG;
          }
        else
          {
            return YAMI_MESSAGE_TYPE_INVALID;
          }
      }
    else
      {
        return YAMI_MESSAGE_TYPE_INVALID;
      }
  }

  int yami_client_send_authenticate (YamiClient *self,
                                     const char *username, const char *password,
                                     const char **tokens, const size_t tokens_length)
  {
    if (self == nullptr) {
      return 0;
    }
    try
      {
        self->data->send_authenticate (username, password,
                                       std::vector<std::string> {tokens, tokens + tokens_length});
        return 1;
      }
    catch (const std::runtime_error &e)
      {
        return 0;
      }
  }

  int yami_client_send_ping (YamiClient *self)
  {
    if (self == nullptr) {
      return 0;
    }
    try
      {
        self->data->send_ping ();
        return 1;
      }
    catch (const std::runtime_error &e)
      {
        return 0;
      }
  }

  int yami_client_send_udp_tunnel (YamiClient *self,
                                   const YamiUdpPacket *udp_packet)
  {
    if (self == nullptr) {
      return 0;
    }
    try
      {
        const auto &packet = *udp_packet;
        Mumble::UdpPacket tmp;
        switch (packet.type)
          {
          case YAMI_UDP_PAYLOAD_TYPE_CELT_ALPHA_AUDIO:
            tmp.type = Mumble::UdpPayloadType::CeltAlpha;
            break;

          case YAMI_UDP_PAYLOAD_TYPE_PING:
            tmp.type = Mumble::UdpPayloadType::Ping;
            break;

          case YAMI_UDP_PAYLOAD_TYPE_SPEEX_AUDIO:
            tmp.type = Mumble::UdpPayloadType::Speex;
            break;

          case YAMI_UDP_PAYLOAD_TYPE_CELT_BETA_AUDIO:
            tmp.type = Mumble::UdpPayloadType::CeltBeta;
            break;

          case YAMI_UDP_PAYLOAD_TYPE_OPUS_AUDIO:
            tmp.type = Mumble::UdpPayloadType::Opus;
            break;

          case YAMI_UDP_PAYLOAD_TYPE_UNUSED1:
            tmp.type = Mumble::UdpPayloadType::Unused1;
            break;

          case YAMI_UDP_PAYLOAD_TYPE_UNUSED2:
            tmp.type = Mumble::UdpPayloadType::Unused2;
            break;

          case YAMI_UDP_PAYLOAD_TYPE_UNUSED3:
            tmp.type = Mumble::UdpPayloadType::Unused3;
            break;
          }
        tmp.target = packet.target;
        tmp.source_session_id = packet.source_session_id;
        tmp.timestamp = packet.timestamp;
        tmp.sequence_number = packet.sequence_number;
        tmp.payload =
          std::vector<uint8_t> (packet.payload,
                                packet.payload + packet.payload_length);
        tmp.terminator = (packet.terminator == 1);
        self->data->send_udp_tunnel (tmp);
        return 1;
      }
    catch (const std::runtime_error &e)
      {
        return 0;
      }
  }

  void yami_client_recv_udp_tunnel (YamiClient *self,
                                    YamiUdpTunnelFunc udp_tunnel_func,
                                    void *user_data)
  {
    if (self == nullptr) {
      return;
    }
    Mumble::UdpPacket packet = self->data->recv_udp_tunnel ();
    YamiUdpPacket tmp;
    switch (packet.type)
      {
      case Mumble::UdpPayloadType::CeltAlpha:
        tmp.type = YAMI_UDP_PAYLOAD_TYPE_CELT_ALPHA_AUDIO;
        break;

      case Mumble::UdpPayloadType::Ping:
        tmp.type = YAMI_UDP_PAYLOAD_TYPE_PING;
        break;

      case Mumble::UdpPayloadType::Speex:
        tmp.type = YAMI_UDP_PAYLOAD_TYPE_SPEEX_AUDIO;
        break;

      case Mumble::UdpPayloadType::CeltBeta:
        tmp.type = YAMI_UDP_PAYLOAD_TYPE_CELT_BETA_AUDIO;
        break;

      case Mumble::UdpPayloadType::Opus:
        tmp.type = YAMI_UDP_PAYLOAD_TYPE_OPUS_AUDIO;
        break;

      case Mumble::UdpPayloadType::Unused1:
        tmp.type = YAMI_UDP_PAYLOAD_TYPE_UNUSED1;
        break;

      case Mumble::UdpPayloadType::Unused2:
        tmp.type = YAMI_UDP_PAYLOAD_TYPE_UNUSED2;
        break;

      case Mumble::UdpPayloadType::Unused3:
        tmp.type = YAMI_UDP_PAYLOAD_TYPE_UNUSED3;
        break;
      }
    tmp.target = packet.target;
    tmp.source_session_id = packet.source_session_id;
    tmp.timestamp = packet.timestamp;
    tmp.sequence_number = packet.sequence_number;
    tmp.payload = packet.payload.data ();
    tmp.payload_length = packet.payload.size ();
    tmp.terminator = packet.terminator ? 1 : 0;
    udp_tunnel_func (&tmp, user_data);
  }
}
