/* -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
  Mumble to Mumble gateway
  Copyright (c) 2017 Phobos (promi) <prometheus@unterderbruecke.de>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <cstdint>
#include <unordered_map>
#include <typeinfo>
#include <typeindex>
#include <memory>
#include <google/protobuf/message.h>

#include "Mumble.pb.h"

namespace Mumble
{
  enum class MessageType : uint16_t
    {
      Version = 0,
      UDPTunnel = 1,
      Authenticate = 2,
      Ping = 3,
      Reject = 4,
      ServerSync = 5,
      ChannelRemove = 6,
      ChannelState = 7,
      UserRemove = 8,
      UserState = 9,
      BanList = 10,
      TextMessage = 11,
      PermissionDenied = 12,
      ACL = 13,
      QueryUsers = 14,
      CryptSetup = 15,
      ContextActionModify = 16,
      ContextAction = 17,
      UserList = 18,
      VoiceTarget = 19,
      PermissionQuery = 20,
      CodecVersion = 21,
      UserStats = 22,
      RequestBlob = 23,
      ServerConfig = 24,
      SuggestConfig = 25
    };

  class Messages
  {
  public:
    static std::unordered_map<std::type_index, uint16_t> sym_to_type;
    static std::unique_ptr<::google::protobuf::Message> msg_from_type (
      uint16_t type);
  };

}
