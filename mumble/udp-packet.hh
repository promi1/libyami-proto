/* -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
  Mumble to Mumble gateway
  Copyright (c) 2017 Phobos (promi) <prometheus@unterderbruecke.de>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <cstdint>

#include "mumble/packet-data-stream.hh"

namespace Mumble
{
  // In the UDP packet it is encoded as a 3 bit code (so ranging 0..7 unsigned)
  enum class UdpPayloadType : uint8_t
  {
    CeltAlpha = 0,
    Ping = 1,
    Speex = 2,
    CeltBeta = 3,
    Opus = 4,
    Unused1 = 5,
    Unused2 = 6,
    Unused3 = 7
  };
  
  class UdpPacket
  {
  public:
    UdpPayloadType type;
    uint8_t target;
    uint32_t source_session_id;
    uint32_t timestamp; // Only used for Ping
    uint32_t sequence_number;
    std::vector<uint8_t> payload;
    bool terminator; // Only used for Opus
    /*
    struct
    {
      float x;
      float y;
      float z;
    } position_info;
    */

    std::vector<uint8_t> data (PacketDataStream &pds) const;
    void data (PacketDataStream &pds, const std::vector<uint8_t> &data);
  };
}
