/* -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
  Mumble to Mumble gateway

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "mumble/udp-packet.hh"

#include <cassert>
#include <stdexcept>

namespace Mumble
{
  std::vector<uint8_t> UdpPacket::data (PacketDataStream &pds) const
  {
    pds.rewind ();
    uint8_t header = (static_cast<uint8_t> (this->type) << 5) |
                     (this->target & 0x1F);
    pds.append (header);
    if (this->type == UdpPayloadType::Ping)
      {
        pds.put_int (this->timestamp);
      }
    else
      {
        pds.put_int (this->sequence_number);
        if (this->type == UdpPayloadType::Opus)
          {
            int64_t len = this->payload.size ();
            assert (len < 0x2000);
            if (this->terminator)
              {
                len |= 0x2000;
              }
            pds.put_int (len);
            pds.append_block (this->payload);
          }
        else
          {
            throw std::runtime_error ("packet type not yet supported");
          }
      }
    auto packet_size = pds.size ();
    pds.rewind ();
    return pds.get_block (packet_size);
  }

  void UdpPacket::data (PacketDataStream &pds, const std::vector<uint8_t> &data)
  {
    pds.rewind ();
    pds.append_block (data);
    pds.rewind ();

    uint8_t header = pds.get_next ();
    this->type = static_cast<UdpPayloadType> (header >> 5);
    this->target = header & 0x1F;

    if (this->type == UdpPayloadType::Ping)
      {
        this->timestamp = pds.get_int ();
      }
    else
      {
        this->source_session_id = pds.get_int ();
        this->sequence_number = pds.get_int ();
        if (this->type == UdpPayloadType::Opus)
          {
            auto len = pds.get_int ();
            this->payload = pds.get_block (len & 0x1FFF);
            this->terminator = ((len & 0x2000) == 0x2000);
          }
        else
          {
            throw std::runtime_error ("packet type not yet supported");
          }
      }
  }
}
