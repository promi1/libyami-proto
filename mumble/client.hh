/* -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
  Mumble to Mumble gateway
  Copyright (c) 2017 Phobos (promi) <prometheus@unterderbruecke.de>
  Copyright (c) 2018 Phobos (promi) <prometheus@unterderbruecke.de>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <string>
#include <cstdint>
#include <memory>
#include <vector>
#include <functional>

#include "log/logger.hh"
#include "mumble/messages.hh"
#include "mumble/udp-packet.hh"
#include "mumble/version.hh"

namespace Mumble
{
  class IClient
  {
  public:
    virtual ~IClient () {}

    virtual std::size_t get_next_read_size () = 0;
    virtual void consume_read_data (const std::vector<std::uint8_t> &data) = 0;
    virtual bool has_message () = 0;
    virtual MessageType message_type () = 0;

    virtual void send_version () = 0;
    virtual Version recv_version () = 0;

    virtual void send_authenticate (const std::string &username,
                                    const std::string &password,
                                    const std::vector<std::string> &tokens) = 0;
    virtual void send_ping () = 0;

    virtual void send_udp_tunnel (const UdpPacket &udp_packet) = 0;
    virtual UdpPacket recv_udp_tunnel () = 0;
  };

  class Client : public IClient
  {
  public:
    Client (const Log::ILogger &logger,
            std::function<void (std::vector<std::uint8_t>)> write_data_func);
    ~Client () override;

    std::size_t get_next_read_size () override;
    void consume_read_data (const std::vector<std::uint8_t> &data) override;
    bool has_message () override;
    MessageType message_type () override;

    void send_version () override;
    Version recv_version () override;

    void send_authenticate (const std::string &username,
                                    const std::string &password,
                                    const std::vector<std::string> &tokens) override;
    void send_ping () override;

    void send_udp_tunnel (const UdpPacket &udp_packet) override;
    UdpPacket recv_udp_tunnel () override;
  private:
    struct Impl;
    std::unique_ptr<Impl> pimpl;
  };
}
