/* -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
  Mumble to Mumble gateway
  Copyright (c) 2018 Phobos (promi) <prometheus@unterderbruecke.de>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <cstdint>
#include <optional>
#include <string>

namespace Mumble
{
  struct Version
  {
    std::optional<uint16_t> major;
    std::optional<uint8_t> minor;
    std::optional<uint8_t> patch;
    std::optional<std::string> release;
    std::optional<std::string> os;
    std::optional<std::string> os_version;
  };
}
