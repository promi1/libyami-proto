/* -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
  Mumble to Mumble gateway
  Copyright (c) 2017 Phobos (promi) <prometheus@unterderbruecke.de>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "endian/utils.hh"
#include "mumble/client.hh"
#include "mumble/messages.hh"
#include "mumble/packet-data-stream.hh"

#include <cassert>
#include <sstream>
#include <functional>
#include <chrono>
#include <type_traits>

using namespace std::literals;

namespace Mumble
{
  struct Client::Impl
  {
    static const uint16_t invalid_type = 0xFFFFu;
    
    const Log::ILogger &logger;
    std::function<void (std::vector<std::uint8_t>)> write_data_func;
    bool is_prefix = false;
    std::uint16_t type = invalid_type;
    bool next_is_prefix = true;
    std::uint16_t next_type = invalid_type;
    std::size_t next_read_size = 6;
    std::unique_ptr<::google::protobuf::Message> message;
    std::vector<uint8_t> udp_payload;
    
    Impl (const Log::ILogger &logger,
          std::function<void (std::vector<std::uint8_t>)> write_data_func)
      : logger (logger), write_data_func (write_data_func)
    {
    }

    void consume_payload (std::size_t type, const std::vector<std::uint8_t> &data);
    static std::vector<uint8_t> make_prefix (uint16_t type, uint32_t len);
    void write_data (const std::vector<uint8_t> &data);
    void send_data (uint16_t type, const std::vector<uint8_t> &data);
    void send_message (uint16_t type, const ::google::protobuf::Message &msg);
    void send (int type, const ::google::protobuf::Message& msg);
    template <typename T>
    inline void send (const T &msg);
    static uint32_t encode_version (uint16_t major, uint8_t minor, uint8_t patch);
  };
  
  void Client::Impl::consume_payload (std::size_t type, const std::vector<std::uint8_t> &data)
  {
    message = Messages::msg_from_type (type);
    if (type == Messages::sym_to_type.at (std::type_index (typeid (
                                                                   MumbleProto::UDPTunnel))))
      {
        // UDP Packet -- No Protobuf
        message = nullptr;
        udp_payload.assign (data.begin (), data.end ());
      }
    else
      {
        std::stringstream is (std::string (std::begin (data), std::end (data)));
        message->ParseFromIstream (&is);
        udp_payload.clear ();
      }
  }

  std::vector<uint8_t> Client::Impl::make_prefix (uint16_t type, uint32_t len)
  {
    std::vector<uint8_t> prefix;
    EndianUtils::add_to_u16be (prefix, type);
    EndianUtils::add_to_u32be (prefix, len);
    return prefix;
  }

  void Client::Impl::write_data (const std::vector<uint8_t> &data)
  {
    write_data_func (data);
  }

  void Client::Impl::send_data (uint16_t type,
                                const std::vector<uint8_t> &data)
  {
    uint32_t len = data.size ();
    auto prefix = make_prefix (type, len);
    write_data (prefix);
    write_data (data);
  }

  void Client::Impl::send_message (uint16_t type,
                                   const ::google::protobuf::Message &msg)
  {
    std::stringstream os;
    msg.SerializeToOstream (&os);
    std::string os_str = os.str ();
    send_data (type, std::vector<uint8_t> (std::begin (os_str),
                                           std::end (os_str)));
  }

  void Client::Impl::send (int type, const ::google::protobuf::Message& msg)
  {
    send_message (type, msg);
  }

  template <typename T>
  inline void Client::Impl::send (const T &msg)
  {
    send (Messages::sym_to_type.at (std::type_index (typeid (T))), msg);
  }

  uint32_t Client::Impl::encode_version (uint16_t major, uint8_t minor, uint8_t patch)
  {
    return (major << 16) | (minor << 8) | (patch & 0xff);
  }

  Client::Client (const Log::ILogger &logger,
                  std::function<void (std::vector<std::uint8_t>)> write_data_func)
    : pimpl(std::make_unique<Impl> (logger, write_data_func))
  {
  }
  
  Client::~Client ()
  {
  }
  
  std::size_t Client::get_next_read_size ()
  {
    return pimpl->next_read_size;
  }

  void Client::consume_read_data (const std::vector<std::uint8_t> &data)
  {
    //LOG(pimpl->logger, std::to_string (data.size ()));
    //LOG(pimpl->logger, std::to_string (pimpl->next_read_size));
    //LOG(pimpl->logger, std::to_string (pimpl->next_is_prefix));
    //LOG(pimpl->logger, std::to_string (pimpl->next_type));
    if (data.size () != pimpl->next_read_size)
      {
        throw std::runtime_error ("Unexpected data size");
      }
    if (pimpl->next_is_prefix)
      {
        pimpl->is_prefix = true;
        pimpl->type = Impl::invalid_type;

        pimpl->next_is_prefix = false;
        pimpl->next_type = EndianUtils::value_from_u16be (data, 0);
        pimpl->next_read_size = EndianUtils::value_from_u32be (data, 2);
      }
    else
      {
        pimpl->is_prefix = false;
        pimpl->type = pimpl->next_type;
        
        pimpl->next_is_prefix = true;
        pimpl->next_type = Impl::invalid_type;
        pimpl->next_read_size = 6;

        pimpl->consume_payload (pimpl->type, data);
      }
  }

  bool Client::has_message ()
  {
    if (pimpl->is_prefix)
      {
        return false;
      }
    else
      {
        if (pimpl->type == Impl::invalid_type)
          {
            return false;
          }
        else
          {
            return true;
          }
      }
  }

  MessageType Client::message_type ()
  {
    return static_cast<MessageType> (pimpl->type);
  }

  void Client::send_version ()
  {
    MumbleProto::Version msg;
    msg.set_version (Impl::encode_version (1, 3, 0));
    msg.set_release ("yami-client 0.1");
    msg.set_os ("Unknown");
    msg.set_os_version ("Unknown");
    pimpl->send (msg);
  }

  Version Client::recv_version ()
  {
    Version version;
    auto &msg = dynamic_cast<MumbleProto::Version &>(*(pimpl->message));
    if (msg.has_version ())
      {
        auto val = msg.version ();
        version.major = val >> 16;
        version.minor = (val & 0xFF00) >> 8;
        version.patch = val & 0xFF;
      }
    if (msg.has_release ())
      {
        version.release = msg.release ();
      }
    if (msg.has_os ())
      {
        version.os = msg.os ();
      }
    if (msg.has_os_version ())
      {
        version.os_version = msg.os_version ();
      }
    return version;
  }

  void Client::send_authenticate (const std::string &username,
                                  const std::string &password,
                                  const std::vector<std::string> &tokens)
  {
    MumbleProto::Authenticate msg;
    msg.set_username (username);
    msg.set_password (password);
    for (auto &token : tokens)
      {
        msg.add_tokens (token);
      }
    msg.set_opus (true);
    pimpl->send (msg);
  }
  
  void Client::send_ping ()
  {
    using namespace std::chrono;
    MumbleProto::Ping msg;
    msg.set_timestamp (duration_cast<milliseconds>
                       (system_clock::now ().time_since_epoch ()).count ());
    pimpl->send (msg);
  }

  void Client::send_udp_tunnel (const UdpPacket &udp_packet)
  {
    PacketDataStream pds;
    auto data = udp_packet.data (pds);
    pimpl->send_data (Messages::sym_to_type.at (std::type_index (typeid (MumbleProto::UDPTunnel))), data);
  }

  UdpPacket Client::recv_udp_tunnel ()
  {
    PacketDataStream pds;
    UdpPacket packet;
    packet.data (pds, pimpl->udp_payload);
    return packet;
  }
}
